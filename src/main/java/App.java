//import com.sun.org.slf4j.internal.LoggerFactory;


import java.io.*;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;
import java.util.logging.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;


public class App {
    static Logger logger = java.util.logging.Logger.getLogger(App.class.getName());
    final static String PROPERTIES = "src/main/resources/app.properties";
    final static String FILE_HANDLER = "src/main/resources/logs/";
    static ArrayList<String> oldNames = new ArrayList<>();
    static ArrayList<String> newNames = new ArrayList<>();
    final static String TIME_STAMP = new SimpleDateFormat("MM_dd__HH_mm_ss").format(new Date());

    public static void main(String[] args) throws IOException, InterruptedException, TransformerException, ParserConfigurationException {
        long startTime = System.currentTimeMillis();
        FileHandler fh = new FileHandler(FILE_HANDLER + TIME_STAMP);
        fh.setFormatter(new MyFormatter());
        logger.addHandler(fh);
        logger.info("Start project");
        FileInputStream fis = new FileInputStream(PROPERTIES);
        Properties properties = new Properties();
        properties.load(fis);
        fis.close();
        CreateFileXML createFileXML = new CreateFileXML();
        if (fileExist(properties)) {
            logger.info("All files is exist!!");
            Set<String> propertiesNames = properties.stringPropertyNames();
            for (String propertieName : propertiesNames) {
                String path = properties.getProperty(propertieName);
                fileConfigRename(path, propertieName, createFileXML);
            }
        } else {
            logger.info("All file is not exist!");
        }
        logger.info("Finish project");
        fh.close();
        CreateFileJSON.PrintFileJSON();
        long stopTime = System.currentTimeMillis();
        createFileXML.setTimer(stopTime - startTime);
        createFileXML.printXML2();
    }

    public static void fileConfigRename(String path, String propName, CreateFileXML cfx) throws IOException {
        FileInputStream fis2 = new FileInputStream(path);
        Properties properties2 = new Properties();
        properties2.load(fis2);

        PropertiesX p = new PropertiesX();
        p.setPropertyX(propName);
        p.setValueX(path);
        fis2.close();
        for (String properties2Name : properties2.stringPropertyNames()) {
            String newName = newName(properties2Name);
            p.putOldNew(properties2Name, newName);
            properties2.setProperty(newName, properties2.getProperty(properties2Name));
            if (!newName.equals(properties2Name)) {
                properties2.remove(properties2Name);
                FileOutputStream fos = new FileOutputStream(path);
                properties2.store(fos, null);
                fos.close();
            }
        }
        cfx.setPropertyToProperyyX(p);
    }

    public static boolean fileExist(Properties properties) throws IOException {

        boolean exist = true;
        for (String propertieName : properties.stringPropertyNames()) {
            File file = new File(properties.getProperty(propertieName));
            if (!(file.exists())) {
                exist = false;
            }
        }
        return exist;
    }

    public static String newName(String name) {
        String[] list = name.split("_");
        StringBuilder sb = new StringBuilder();
        sb.append("NEW_NAME_");
        sb.append(list[list.length - 1]);
        logger.info("File " + name + " rename to " + sb.toString());
        return sb.toString();
    }

    static class MyFormatter extends XMLFormatter {
        @Override
        public String format(LogRecord record) {
            StringBuilder sb = new StringBuilder(500);
            sb.append("<record>\n");
            final Instant instant = record.getInstant();
            sb.append("  <date>");
            sb.append(instant);
            sb.append("</date>\n");
            sb.append("  <level>");
            sb.append(record.getLevel());
            sb.append("</level>\n");
            sb.append("  <message>");
            sb.append(record.getMessage());
            sb.append("</message>");
            sb.append("\n");
            sb.append("</record>\n");
            return sb.toString();
        }
    }
}
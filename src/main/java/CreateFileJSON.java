import org.json.JSONObject;
import org.json.XML;

import java.io.*;
import java.util.Scanner;

public class CreateFileJSON {
    static final String JSON_FILE = "src/main/resources/ResultFilesJSON/jsonlog" + App.TIME_STAMP + ".json";

    public static void PrintFileJSON() throws IOException {
        FileInputStream fis = new FileInputStream(App.FILE_HANDLER + App.TIME_STAMP);
        Scanner scanner = new Scanner(fis);
        StringBuilder sb = new StringBuilder();
        while (scanner.hasNextLine()) {
            sb.append(scanner.nextLine() + "\n");
        }
        JSONObject xmlJSONObj = XML.toJSONObject(sb.toString());
        FileWriter fw = new FileWriter(JSON_FILE);
        fw.write(xmlJSONObj.toString());
        fw.close();
    }
}

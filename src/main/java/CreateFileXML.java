import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.ArrayList;
import java.util.Properties;


public class CreateFileXML {

    Properties properties;
    ArrayList<String> oldNames;
    ArrayList<String> newNames;
    ArrayList<PropertiesX> properList = new ArrayList<>();
    private long timer;
    static final String XML_FILES = "src/main/resources/ResultFilesXML/" + App.TIME_STAMP + ".xml";

    public CreateFileXML() {
    }
    public void printXML2() throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        Document doc = dbf.newDocumentBuilder().newDocument();
        File file = new File(XML_FILES);
        Element root = doc.createElement("Task");
        root.setAttribute("executionTime", Long.toString(timer));
        doc.appendChild(root);
        for (PropertiesX prop : getProperList()) {
            Element propertiesFilesNames = doc.createElement("PropertiesFiles");
            propertiesFilesNames.setTextContent(prop.getPropertyX() + "=" + prop.getValueX());
            root.appendChild(propertiesFilesNames);
            for (String keyName : prop.getOldNewX().keySet()) {
                Element config = doc.createElement("Config");
                propertiesFilesNames.appendChild(config);
                Element oldNames = doc.createElement("OldNames");
                oldNames.setTextContent(keyName);
                config.appendChild(oldNames);
                Element newName = doc.createElement("NewName");
                newName.setTextContent(prop.getOldNewX().get(keyName));
                config.appendChild(newName);


            }
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(new DOMSource(doc), new StreamResult(file));

        }
    }

    public Properties getProperties() {
        return properties;
    }

    public ArrayList<String> getOldNames() {
        return oldNames;
    }

    public ArrayList<String> getNewNames() {
        return newNames;
    }

    public ArrayList<PropertiesX> getProperList() {
        return properList;
    }

    public long getTimer() {
        return timer;
    }

    public static String getXmlFiles() {
        return XML_FILES;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public void setOldName(String oldName) {
        oldNames.add(oldName);
    }

    public void newOldName(String newName) {
        newNames.add(newName);

    }

    public void setProperList(ArrayList<PropertiesX> properList) {
        this.properList = properList;
    }

    public void setPropertyToProperyyX(PropertiesX prop) {

        properList.add(prop);
    }

    public void setOldNames(ArrayList<String> oldNames) {
        this.oldNames = oldNames;
    }

    public void setNewNames(ArrayList<String> newNames) {
        this.newNames = newNames;
    }

    public void setTimer(long timer) {
        this.timer = timer;
    }
}
